#!/bin/sh
# Filename: flow-part2.sh
# Startdate: 2020-05-30 16:46
# Purpose: automate the post-download components of the issue-saving process

OUTDIR=/mnt/public/www/gitlab-issues

cd /mnt/public/work/devuan

sed -i -r -e 's/\\n/\n/g;' "${OUTDIR}"/*.html

ls -1 "${OUTDIR}"/*.html > output/files-for-timestamps.txt
./fix-timestamps.py

DEBUG=1 ./fetch-images.sh 2>~/log/flow-part2.$( date "+%FT%H%M%S" ).log
sed -i -f fix-images-in-html.sed "${OUTDIR}"/*.html

mkdir -p "${OUTDIR}/.css"
./fetch-css.sh
sed -i -f fix-css-in-html.sed "${OUTDIR}"/*.html

sed -i -f remove-useless.sed "${OUTDIR}"/*.html

./remove-useless.py

sed -i -r -f fix-without-systemd-links.sed "${OUTDIR}"/*.html

./conversion.sh "${OUTDIR}"/*.html

./use-datasrc-instead-src.py

./fix-issue-links.sh
sed -i -f fix-issue-links.sed "${OUTDIR}"/*.html
