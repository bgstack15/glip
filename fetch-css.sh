#!/bin/sh
# Startdate: 2020-05-29 20:18

INDIR=/mnt/public/www/gitlab-issues
INGLOB=*.html

SEDSCRIPT=/mnt/public/work/devuan/fix-css-in-html.sed

# OUTDIR will be made in INDIR, because of the `cd` below.
OUTDIR=.css
test ! -d "${OUTDIR}" && mkdir -p "${OUTDIR}"

INSERVER=https://git.devuan.org

cd "${INDIR}"

#orig_css="$( sed -n -r -e 's/^.*rel="stylesheet".*(href="[^"]+\.css").*/\1/p' ${INGLOB} | awk -F'"' '!x[$2]++{print $2}' )"
orig_css="$( sed -n -r -e 's/^.*<link.*(href="[^"]+\.css").*/\1/p' ${INGLOB} | awk -F'"' '!x[$2]++{print $2}' )"

cat /dev/null > "${SEDSCRIPT}"

echo "${orig_css}" | while read line ; do
   getpath="${INSERVER}/${line}"
   targetfile="${OUTDIR}/$( basename "${line}" )"
   targetfile="${targetfile##/}"
   targetfile=".$( echo "${targetfile}" | sed -r -e 's/^\.+//g;' )"
   test -n "${DEBUG}" && echo "process ${getpath} and save to ${targetfile}" 1>&2
   test -z "${DRYRUN}" && wget --quiet --content-disposition -O "${targetfile}" "${getpath}"
   # dynamically build a sed script
   echo "s:${line}:${targetfile}:g;" | tee -a "${SEDSCRIPT}"
done
