$ {s/^'//}
1 {s/^b'//}
s/Â·/·/g # do not ask how I made this one
s/Ã//g
s/\\'/'/g
s/\xc2(\x91|\x82|\x)//g
s/\\xc2\\xb7/·/g # two characters here
s/\\xc3\\xab/�/g
s/\\xe1\\xb4\\x84\\xe1\\xb4\\xa0\\xe1\\xb4\\x87/CVE/g
s/\\xe2\\x80\\x99/'/g
s/\\xe2\\x80\\xa6/.../g
s/(\\x..)*\\xb7/·/g # two characters here
s/\xc3.·/·/g # do not ask how I made this one
