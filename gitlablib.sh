#!/bin/sh
# Startdate: 2020-05-29
# Dependencies:
#    jq
#    my private token
# Library for interacting with Gitlab API
# For manual work:
#    curl --header "${authheader}" "https://git.devuan.org/api/v4/projects/devuan%2Fdevuan-project/issues"
# References:
#    https://docs.gitlab.com/ee/api/README.html#pagination
#    handle transforming the / in the path_with_namespace to %2F per https://docs.gitlab.com/ee/api/README.html#namespaced-path-encoding https://docs.gitlab.com/ee/api/README.html#namespaced-path-encoding
#    https://docs.gitlab.com/ee/api/issues.html

export token="$( cat /mnt/public/work/devuan/git.devuan.org.token.txt )"
export authheader="Private-Token: ${token}"

export server=git.devuan.org

export GLL_TMPDIR="$( mktemp -d )"

clean_gitlablib() {
   rm -rf "${GLL_TMPDIR:-NOTHINGTODELETE}"/*
}

# PRIVATE
_handle_gitlab_pagination() {
   # call: list_all_projects "${startUri}"
   ___hgp_starturi="${1}"
   test -n "${GLL_DEBUG}" && set -x
   # BEGIN
   rhfile="$( TMPDIR="${GLL_TMPDIR}" mktemp -t "headers.XXXXXXXXXX" )"
   done=0
   size=-1
   uri="${___hgp_starturi}"

   # LOOP
   while test ${done} -eq 0 ;
   do
      response="$( curl -v -L --header "${authheader}" "${uri}" 2>"${rhfile}" )" 
      #grep -iE "^< link" "${rhfile}"
      # determine size
      if test "${size}" = "-1" ; then # run only if size is still undefined
         tmpsize="$( awk '$2 == "x-total:" {print $3}' "${rhfile}" 2>/dev/null )"
         test -n "${tmpsize}" && size="${tmpsize}"
         echo "Number of items: ${size}" 1>&2
      fi

      tmpnextpage="$( awk '$2 == "x-next-page:" {print $3}' "${rhfile}" 2>/dev/null )"
      # if x-next-page is blank, that means we are on the last page. Also, we could try x-total-pages compared to x-page.
      test -z "${tmpnextpage}" && done=1
      # so if we have a next page, get that link
      nextUri="$( awk '{$1="";$2="";print}' "${rhfile}" | tr ',' '\n' | awk -F';' '/rel="next"/{print $1}' | sed -r -e 's/^\s*<//;' -e 's/>\s*$//;' )"
      if test -n "${nextUri}" ; then
         uri="${nextUri}"
      else
         echo "No next page provided! Error." 1>&2
         done=1
      fi

      # show contents
      echo "${response}"
   done

   # cleanup
   rm "${rhfile}"
   set +x
}

list_all_projects() {
   _handle_gitlab_pagination "https://${server}/api/v4/projects"
}

list_all_issues() {
   _handle_gitlab_pagination "https://${server}/api/v4/issues?scope=all&status=all"
}

list_issues_for_project() {
   ___lifp_project="${1}"
   ___lifp_htmlencode_bool="${2}"
   istruthy "${___lifp_htmlencode_bool}" && ___lifp_project="$( echo "${___lifp_project}" | sed -r -e 's/\//%2F/g;' )"
   _handle_gitlab_pagination "https://${server}/api/v4/projects/${___lifp_project}/issues"
}

list_issues_for_all_projects_pipe() {
   # call: <projects.path_with_namespace.txt list_issues_for_all_projects_pipe
   echo "STUB"
}

