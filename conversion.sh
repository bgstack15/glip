# startdate 2020-06-15
# References:
#    iconv(1), charsets(7)
for oldfile in $@ ; do
   test -w "${oldfile}" && {
      newfile="${oldfile}.2"
      iconv -c -f utf-8 -t iso-8859-1 < "${oldfile}" > "${newfile}"
      mv "${newfile}" "${oldfile}"
   }
done
